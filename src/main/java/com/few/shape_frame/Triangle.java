/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape_frame;

/**
 *
 * @author f
 */
public class Triangle extends Shape {

    private double h;
    private double b;

    public Triangle(double h, double b) {
        super("Triangle");
        this.h = h;
        this.b = b;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getH() {
        return h;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getB() {
        return b;
    }

    @Override
    public double calArea() {
        return 0.5 * h * b;
    }

    @Override
    public double calPerimeter() {
        double c = Math.sqrt((Math.pow(b, 2) + Math.pow(h, 2)));
        return c + b + h;
    }

}
