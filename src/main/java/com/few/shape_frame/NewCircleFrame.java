/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author f
 */
public class NewCircleFrame extends JFrame{
    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;
    
    public NewCircleFrame() {
        super("Circle");
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblRadius = new JLabel("Radius: ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);
        
        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Circle Radius = ???, Area = ???, perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRadius = txtRadius.getText();
                    double radius = Double.parseDouble(strRadius);
                    Circle circle = new Circle(radius);
                    lblResult.setText("Circle Radius = " + String.format("%.2f", circle.getRadius())
                            + ", Area = " + String.format("%.2f", circle.calArea())
                            + ", Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(NewCircleFrame.this, "Error: Please input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }
    public static void main(String[] args) {
        NewCircleFrame frame = new NewCircleFrame();
        frame.setVisible(true);
    }
}
