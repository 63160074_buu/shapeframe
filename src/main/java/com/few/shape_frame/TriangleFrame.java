/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author f
 */
public class TriangleFrame extends JFrame {
    JLabel lblBase, lblHigh;
    JTextField txtBase, txtHigh;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblBase = new JLabel("Wide: ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        lblHigh = new JLabel("High: ", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setLocation(5, 30);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        this.add(lblHigh);

        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);
        
        txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 30);
        this.add(txtHigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle Base = ???, High = ???, Area = ???, Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 70);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHigh = txtHigh.getText();
                    double b = Double.parseDouble(strBase);
                    double h = Double.parseDouble(strHigh);
                    Triangle triangle = new Triangle(h, b);
                    lblResult.setText("Rectangle Base = " + String.format("%.2f", triangle.getB())
                            + ", High = " + String.format("%.2f", triangle.getH())
                            + ", Area = " + String.format("%.2f", triangle.calArea())
                            + ", Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                }
            }
        });
    }
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
