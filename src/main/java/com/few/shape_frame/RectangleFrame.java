/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape_frame;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author f
 */
public class RectangleFrame extends JFrame {

    JLabel lblWide, lblHigh;
    JTextField txtWide, txtHigh;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWide = new JLabel("Wide: ", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        this.add(lblWide);
        
        lblHigh = new JLabel("High: ", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setLocation(5, 30);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        this.add(lblHigh);

        txtWide = new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        this.add(txtWide);
        
        txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 30);
        this.add(txtHigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle Wide = ???, High = ???, Area = ???, Perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 70);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWide = txtWide.getText();
                    String strHigh = txtHigh.getText();
                    double w = Double.parseDouble(strWide);
                    double h = Double.parseDouble(strHigh);
                    Rectangle rectangle = new Rectangle(w, h);
                    lblResult.setText("Rectangle Wide = " + String.format("%.2f", rectangle.getW())
                            + ", High = " + String.format("%.2f", rectangle.getH())
                            + ", Area = " + String.format("%.2f", rectangle.calArea())
                            + ", Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                }
            }
        });
    }
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
