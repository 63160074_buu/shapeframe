/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.shape_frame;

/**
 *
 * @author f
 */
public class Square extends Shape {

    private double s;

    public Square(double s) {
        super("Square");
        this.s = s;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getS() {
        return s;
    }

    @Override
    public double calArea() {
        return s * s;
    }

    @Override
    public double calPerimeter() {
        return s * 4;
    }

}
